package main

import (
	"fmt"
)

func main() {
	var Links []LinksType
	var DoneLinks []LinksType
	chanOutputLinks := make(chan LinksType)
	url := "http://suagacollection.com"
	
	ReadLinks(&Links)
	
	for i:=0 ; i<50; i++ {
		go TestLink(Links, chanOutputLinks, url)
		DoneLinks = append(DoneLinks,<-chanOutputLinks)
	}
	
	results, count, connects := socketMain(DoneLinks)
	for i := 0; i < count; i++ {
		answear := <- results
		if answear == "false"{
			fmt.Println("Client got: " + answear)		
		} else {
			parseAnswear(answear)
		}
	}
	CloseConnects(connects)
}
		
func checkerr(e error) {
	if e != nil {
		fmt.Println(e)
	}
}