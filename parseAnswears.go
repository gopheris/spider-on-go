package main

import (
	"encoding/json"
	"fmt"
)
func parseAnswear(answear string){
	answ:=[]Answer{}
	err:=json.Unmarshal([]byte(answear),&answ)
	if err!=nil{fmt.Println(err)}
	fmt.Print("Client got: ",answ,"\n")
}
type Param struct{
	Id string `json:"param"`
	T  string `json:"t"`
}
type I struct{
	T string `json:"t"`
	Ti string `json:"ti"`
	P  string  `json:"p"`
}
type Answer struct{
	P Param `json:"params"` 
	Vulns []I `json:"i"`
}